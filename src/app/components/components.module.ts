import { NgModule } from '@angular/core';
import { NoticiasComponent } from './noticias/noticias.component';
import { CommonModule } from '@angular/common';
@NgModule({
  imports:[CommonModule],
  declarations: [NoticiasComponent],
  exports: [NoticiasComponent],
})
export class ComponentsModule {}
