import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';

import { Tab1PageRoutingModule } from './tab1-routing.module';
import { NoticiasComponent } from 'src/app/components/noticias/noticias.component';
import { NoticiaComponent } from '../../components/noticia/noticia.component';

import { InfiniteScrollComponent } from '../../components/infinite-scroll/infinite-scroll.component';

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule, Tab1PageRoutingModule],
  declarations: [
    Tab1Page,
    NoticiasComponent,
    InfiniteScrollComponent,
    NoticiaComponent,
    
  ],
})
export class Tab1PageModule {}
