import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})

export class Tab2Page {
  categorias = [
    'business',
    'entertainment',
    'general',
    'health',
    'science',
    'sports',
    'technology',
  ];

  primera = this.categorias[0];

  noticias: any = [];

  segmentChanged(ev: any) {
    this.dataService.setCategoria(ev.detail.value);
    this.noticias = [];
    this.loadNoticias();
    console.log('Segment changed', ev.detail.value);
  }
  constructor(private dataService: DataService) {}
  ngOnInit() {
    this.loadNoticias();
  }

  loadData(event) {
    this.loadNoticias();
    event.target.complete();
  }

  loadNoticias(event?) {
    this.dataService.getNoticias().subscribe((data) => {
      this.noticias.push(...data.articles);

      if (event) {
        event.target.complete();
      }
    });
  }
}
