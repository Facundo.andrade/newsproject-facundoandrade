import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoticiasComponent } from 'src/app/components/noticias/noticias.component';
import { Tab3Page } from './tab3.page';

const routes: Routes = [
  {
    path: '',
    component: Tab3Page,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: [NoticiasComponent, Tab3Page],
  exports: [RouterModule]
})
export class Tab3PageRoutingModule {}
