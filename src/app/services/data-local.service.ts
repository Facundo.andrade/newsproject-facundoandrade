import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage-angular';
import { Article } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  constructor(private storage: Storage) { this.storage.create(); this.cargarFavoritos(); }

  noticias: Article [] = [];

  guardarNoticia(noticia: Article){

    this.noticias.unshift(noticia);
    this.storage.set('favoritos', this.noticias);

  }

  async cargarFavoritos(){

    const favoritos = await this.storage.get('favoritos');
    if(favoritos){

      this.noticias = favoritos;
      return this.noticias;

    }

  }


}
