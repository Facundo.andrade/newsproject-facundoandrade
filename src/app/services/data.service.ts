import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Noticia } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}
  a: any = [];
  categoria: string = 'general';

  getNoticias() {
    //return this.http.get(`https://newsapi.org/v2/everything?q=Apple&from=2021-04-12&sortBy=popularity&apiKey=${environment.apiKey}`)
    //return this.http.get('https://jsonplaceholder.typicode.com/users');
    return this.http.get<Noticia>(
      `https://newsapi.org/v2/top-headlines?language=es&category=${this.categoria}&apiKey=${environment.apiKey}`
    );
  }
  setCategoria(newCategory) {
    this.categoria = newCategory;
    console.log(this.categoria);
  }
}
